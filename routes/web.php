<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::prefix('admin')->middleware('admin')->group(function(){

    Route::prefix('users')->group(function(){
        Route::get('/', 'AdminController@getUsers')->name('admin.users.get');
        Route::post('/', 'AdminController@createUser')->name('admin.users.create');

        Route::get('{user_id}/requests', 'AdminController@getRequests')->name('admin.requests.get');
        Route::post('{user_id}/requests/{request_id}/{request_status}', 'AdminController@updateRequest')->name('admin.requests.update');

        /* AJAX routes */
        Route::post('grants', 'AdminController@changeUserGrant')->name('admin.users.grants');
    });

});

Route::prefix('user')->middleware('auth')->group(function(){
    Route::get('requests', 'UserController@getRequests')->name('user.requests.get');
    Route::post('requests', 'UserController@createRequest')->name('user.requests.create');
    Route::post('requests/{id}', 'UserController@cancelRequest')->name('user.requests.cancel');
});