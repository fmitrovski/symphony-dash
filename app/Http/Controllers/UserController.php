<?php

namespace App\Http\Controllers;

use App\User;
use App\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * return \Illuminate\Http\Response
     */
    public function getRequests()
    {
        $user = Auth::user();

        return view('user.requests')->with('requests', $user->user_requests);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function createRequest(Request $request)
    {
        $user_request = new UserRequest();
        $user_request->createRequest($request->all());

        return Redirect::route('user.requests.get');
    }

    /**
     * @param Request $request
     * @param $request_id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function cancelRequest(Request $request, $request_id)
    {
        $user_request = new UserRequest();
        $user_request->cancelRequest($request_id);

        return Redirect::route('user.requests.get');
    }
}
