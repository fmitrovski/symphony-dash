<?php

namespace App\Http\Controllers;

use App\Mail\RequestStatusChanged;
use App\Mail\UserCreated;
use App\User;
use App\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class AdminController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * return \Illuminate\Http\Response
     */
    public function getUsers()
    {
        return view('admin.users')->with('users', User::where('role', '=', 'User')->get());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function createUser(Request $request)
    {
        $password = str_random(env('RANDOM_PASSWORD_LENGTH', 5));

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($password);

        $user->save();

        Mail::to($user->email)->queue(new UserCreated($user->email, $password));

        return Redirect::route('admin.users.get');
    }

    /**
     * @param Request $request
     * @param $user_id
     * @return $this
     */

    public function getRequests(Request $request, $user_id)
    {
        $user = User::find($user_id);

        return view('admin.requests')->with('requests', $user->user_requests);
    }

    /**
     * @param Request $request
     * @param $user_id
     * @param $user_request_id
     * @param $request_status
     * @return \Illuminate\Http\RedirectResponse
     */

    public function updateRequest(Request $request, $user_id, $user_request_id, $request_status)
    {
        $user_request = UserRequest::find($user_request_id);
        $user_request->status = $request_status;
        $user_request->save();

        Mail::to($user_request->user->email)->queue(new RequestStatusChanged($user_request->date_requested, strtolower($request_status)));

        return Redirect::route('admin.requests.get', ['user_id' => $user_id]);
    }

    /**
     * AJAX-only method for toggling user grants
     *
     * Key 'field' should be enum: email_access_granted, git_repository_granted, microsoft_office_licence, trello_access_granted
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function changeUserGrant(Request $request)
    {
        try {

            $user = new User();
            $user->changeUserGrant($request->all());

            return response()->json(['success' => true]);
        }
        catch (Exception $e)
        {
            return response()->json(['success' => false, 'message' => $e->getLine()]);
        }
    }
}
