<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_requests()
    {
        return $this->hasMany('App\UserRequest');
    }

    public function pending_user_requests()
    {
        return $this->user_requests()->where('status', '=','Pending');
    }

    public function changeUserGrant($data)
    {
        $field = $data['field']; // get db column to be updated

        $user = User::find($data['user_id']);
        $user->$field = $data['value'];
        $user->save();
    }

}
