<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestStatusChanged extends Mailable
{
    use Queueable, SerializesModels;

    private $date_requested;
    private $status;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($date_requested, $status)
    {
        $this->date_requested = $date_requested;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.requeststatuschanged')->with([
            'date_requested' => $this->date_requested,
            'status' => $this->status
        ]);
    }
}
