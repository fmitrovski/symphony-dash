<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserRequest extends Model
{
    protected $fillable = [
        'user_id', 'date_requested', 'start_hour', 'end_hour', 'status'
    ];

    public function createRequest($data)
    {
        $data['user_id'] = Auth::id();

        UserRequest::create($data);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cancelRequest($request_id)
    {
        $user_request = UserRequest::find($request_id);
        $user_request->delete();
    }

    public function changeRequestStatus($request_id, $status)
    {
        $user_request = UserRequest::find($request_id);
        $user_request->status = $status;
    }
}
