@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Users</div>

                <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date requested</th>
                        <th scope="col">Start hour</th>
                        <th scope="col">End hour</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->id }}</td>
                            <td>{{ $request->date_requested }}</td>
                            <td>{{ $request->start_hour }}</td>
                            <td>{{ $request->end_hour }}</td>
                            <td>{{ $request->status }}</td>
                            <td>
                                @if($request->status == 'Pending') {{-- Allow canceling only if request is pending --}}
                                <form action="{{ route('user.requests.cancel', $request->id) }}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Cancel</button>
                                </form></td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            </div>

            <div class="card">
                <div class="card-header">New request</div>
                <div class="card-body">
                    <div class="container">
                    <form action="{{ route('user.requests.create') }}" method="post">
                        @csrf
                        <div class="form-group col-sm-6">
                            <label for="date">Date</label>
                            <input name="date_requested" type="date" id="date" class="form-control" required>
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="start-hour">Start hour</label>
                            <input name="start_hour" type="number" id="start-hour" class="form-control" value="8" min="0" max="24" required>
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="start-hour">End hour</label>
                            <input name="end_hour" type="number" id="end-hour" class="form-control" value="16" min="0" max="24" required>
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="sick">I'm sick</label>
                            <input name="sick" type="checkbox" id="sick" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Create request</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        Date.prototype.addDays = function(days) {
            this.setDate(this.getDate() + parseInt(days));
            return this;
        };

        /*
        * Prepare date format to YYYY-MM-DD for use with input type='date'
        * */

        function formatDate(date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1; // Jan is 0
            var yy = date.getFullYear();
            // Add leading zeroes to days and months
            if(dd<10){dd='0'+dd}
            if(mm<10){mm='0'+mm}

            return yy+'-'+mm+'-'+dd;
        }

        /*
        * Sets soonest possible date for work-from-home request when not sick
        * Next day if no later than 4 PM, two days later otherwise
        *
        * */

        function setDateMinimum() {
            var now = new Date();

            if(now.getHours() > 16)
                min = now.addDays(2);
            else
                min = now.addDays(1);

            $('#date').attr('min', formatDate(min));
        }


        $(document).ready(function() {
            setDateMinimum();

            /*
            * If sick and earlier than 8 AM, set minimum date to today
            * If sick and later than 8 AM, set minimum date to next day
            *
            * If miraculously healthy again, set defaults
            * */

            $("#sick").change(function() {
                if(this.checked) {
                    var now = new Date();
                    if(now.getHours() < 8)
                        $('#date').attr('min', formatDate(now));
                    else {
                        var min = now.addDays(1);
                        $('#date').attr('min', formatDate(min));
                    }
                }
                else
                {
                    setDateMinimum();
                }
            });
        });
    </script>
@endsection
