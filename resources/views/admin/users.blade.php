@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Users</div>

                <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Email Access</th>
                        <th scope="col">Git Repository</th>
                        <th scope="col">MS Office Licence</th>
                        <th scope="col">Trello Access</th>
                        <th scope="col">Pending Requests</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td><input type="checkbox" {{ $user->email_access_granted ?  ' checked' : '' }} name="email_access_granted" data-user-id="{{ $user->id }}"></td>
                            <td><input type="checkbox" {{ $user->git_repository_granted ?  ' checked' : '' }} name="git_repository_granted" data-user-id="{{ $user->id }}"></td>
                            <td><input type="checkbox" {{ $user->microsoft_office_licence ?  ' checked' : '' }} name="microsoft_office_licence" data-user-id="{{ $user->id }}"></td>
                            <td><input type="checkbox" {{ $user->trello_access_granted ?  ' checked' : '' }} name="trello_access_granted" data-user-id="{{ $user->id }}"></td>
                            <td>{{ $user->pending_user_requests->count() }}</td>
                            <td><a class="btn btn-primary" href="{{ route('admin.requests.get', [$user->id]) }}">See requests</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            </div>

            <div class="card">
                <div class="card-header">New user</div>
                <div class="card-body">
                    <form action="{{ route('admin.users.create') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input name="name" type="text" id="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input name="email" type="email" id="email" class="form-control" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Create user</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function() {
            $(":checkbox").change(function() {

                var value = (this.checked)?1:0;
                var field = this.name;

                var data1 = '_token = <?php echo csrf_token() ?>&value=' + encodeURIComponent(value) + '&field=' + encodeURIComponent(field) + '&user_id=' + encodeURIComponent($(this).attr("data-user-id"));

                $.ajax({
                    type:'POST',
                    url:'/admin/users/grants',
                    data:data1,
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){}
                });
            });
        });
    </script>
@endsection
