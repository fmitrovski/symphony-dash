@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Date requested</th>
                        <th scope="col">Start hour</th>
                        <th scope="col">End hour</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr>
                            <td>{{ $request->date_requested }}</td>
                            <td>{{ $request->start_hour }}</td>
                            <td>{{ $request->end_hour }}</td>
                            <td>{{ $request->status }}</td>
                            <td><form method="post" action="{{ route('admin.requests.update', ['user_id' => $request->user_id, 'request_id' => $request->id, 'request_status' => 'Approved']) }}">@csrf<button type="submit" class="btn btn-success">Approve</button></form></td>
                            <td><form method="post" action="{{ route('admin.requests.update', ['user_id' => $request->user_id, 'request_id' => $request->id, 'request_status' => 'Declined']) }}">@csrf<button type="submit" class="btn btn-danger">Decline</button></form></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a class="btn btn-primary" href="{{ route('admin.users.get') }}">Back to users</a>
            </div>
        </div>
    </div>
@endsection
