## Setting up
---
1. Run composer install
2. Add ADMIN_EMAIL and ADMIN_PASSWORD to your .env file. The seeder will use these to generate the admin account.
3. Set up database and mail server variables in your .env file
4. Run migrations
5. Run seeder AdminSeed
6. You're ready to log in as admin and add users

Note: If not logged in, homepage will redirect to login.

## User creation
---
A random password 5 characters in length will be generated and sent via email to the new user. Password length can be set in RANDOM_PASSWORD_LENGTH

## User login and requests
---
Use details sent via email to login. Users will be redirected to their requests dashboard.

When filing for a work from home request, the "I'm sick" checkbox serves only as a modifier for the calendar restrictions and is not saved in the database.

## Schema scripts
---
Eloquent schema scripts are included in the /database/migrations folder.